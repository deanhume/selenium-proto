var assert = require('assert');
describe('settled home page', function() {
    it('should have the right title', function () {
        browser.url('https://staging.settled.co.uk');
        var title = browser.getTitle();
        assert.equal(title, 'Settled | Sell Your House Online');
    });

    it('should navigate to get started', function () {
        // Arrange
        var result = 'https://staging.settled.co.uk/add-listing/2';
        var urlToNavigateTo = 'https://staging.settled.co.uk';
        var selector = '#edit-submit--5';

        // Act
        browser.url(urlToNavigateTo);
        browser.click(selector);

        // Assert
        var resultingUrl = browser.getUrl();
        assert.equal(resultingUrl, result);
    });
});

// describe('Settled Add Listing - Entry', function() {
//   it('should navigate to next step', function () {
//       // Arrange
//       var urlToNavigateTo = 'https://staging.settled.co.uk/add-listing/2';
//       var selector = '.form-item-field-flat-house';
//       var secondSelector = '#edit-field-property-type-4';
//       var submitButton = '#edit-continue-signup';
//
//       // Act
//       browser.url(urlToNavigateTo);
//       browser.click(selector);
//       // browser.click(secondSelector);
//       browser.waitUntil(function(){
//         return browser.element('.form-radios .form-item:nth-child(1) input');
//       });
//       var elem = browser.element('.form-radios .form-item:nth-child(1) input');
//       browser.click(elem);
//
//       browser.click(submitButton);
//
//       // Assert
//       browser.waitUntil(function () {
//         return browser.getUrl() !== urlToNavigateTo
//       }, 5000, 'expected url to be different after 5s');
//       console.log(browser.getUrl());
//       assert.equal(browser.getUrl() !==urlToNavigateTo);
//   });
// });
